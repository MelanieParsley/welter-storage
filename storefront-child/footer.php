<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */
?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full">

			<div class="site-info">
			<?php echo esc_html( $content = '&copy; ' . get_bloginfo( 'name' ) . ' ' . date( 'Y' ) ); ?>
			<br /> Site designed and powered by <a href="https://www.net-smart.net" alt="Net-Smart" title="Net-Smart" rel="designer">Net-Smart</a>
		    </div><!-- .site-info -->

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
