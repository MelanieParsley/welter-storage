<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; ?>
<div id="spacerak"><img src="http://www.welterstorage.com/wp-content/uploads/2018/05/spacerak.jpg" width="195"></div><br style="clear: both;" />
<div id="pallet-rack-header"><img src="http://www.welterstorage.com/wp-content/uploads/2018/05/pallet-rack-web.jpg"></div><br />
		<?php
			/**
			 * woocommerce_archive_description hook
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
		?>

		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

<table class="related_table category_table">

            <h2>Uprights</h2>
    <?php
        $args = array( 'post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => 'uprights', /*'orderby' => 'ID'*/ );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

                <?php wc_get_template_part( 'newpalletrack', 'product' ); ?>

    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
</table><!--/.products-->

<table class="related_table category_table">

            <h2>Beams</h2>
    <?php
        $args = array( 'post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => 'beams', /*'orderby' => 'ID'*/ );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

                <?php wc_get_template_part( 'newpalletrack', 'product' ); ?>

    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
</table><!--/.products-->

<table class="related_table category_table">

            <h2>Wire Decks</h2>
    <?php
        $args = array( 'post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => 'wire-decks', /*'orderby' => 'ID'*/ );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

                <?php wc_get_template_part( 'newpalletrack', 'product' ); ?>

    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
</table><!--/.products-->

<p>USE THIS CHART TO HELP DETERMINE WHAT QUANTITY AND WIDTH OF WIRE DECKS TO USE WITH SET BEAM LENGTHS<br>
8 FT Beam Levels	Use 2 Wire Decks @ 45-1/2" Wide Per Level x Whatever "Depth" Your Upright Is<br>
9 FT Beam Levels	Use 2 Wire Decks @ 53" Wide Per Level x Whatever "Depth" Your Upright Is<br>
10 FT Beam Levels	Use 2 Wire Decks @ 58" Wide Per Level x Whatever "Depth" Your Upright Is<br>
12 FT Beam Levels	Use 3 Wire Decks @ 45-1/2" Wide Per Level x Whatever "Depth" Your Upright Is</p>

<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' ); ?>