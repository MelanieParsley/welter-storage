<?php

/** Remove Product Reviews **/

add_filter( 'woocommerce_product_tabs', 'wcs_woo_remove_reviews_tab', 98 );
function wcs_woo_remove_reviews_tab($tabs) {
 unset($tabs['reviews']);
 return $tabs;
}

/** Remove secondary navigation and original product search in header and replace with new that includes phone number image **/

add_action( 'init', 'sf_remove_and_add', 15);
function sf_remove_and_add() {
	remove_action( 'storefront_header', 'storefront_secondary_navigation', 30);
	remove_action( 'storefront_header', 'storefront_product_search', 40 );
	add_action('storefront_header', 'storefront_product_search_and_phone', 40 );
}

/**
 * Display Product Search and Phone Number
 * @since 1.0.0
 * @uses is_woocommerce_activated() check if WooCommerce is activated
 * @return void
 */
 
if ( ! function_exists( 'storefront_product_search_and_phone') ) {
    function storefront_product_search_and_phone() {
        //if ( is_woocommerce_activated() ) { ?>
            <div class="site-search">
                <?php the_widget( 'WC_Widget_Product_Search', 'title=' ); ?>
                <div class="header-phone"><a href="tel:18006574347"><img src="https://www.welterstorage.com/wp-content/uploads/2015/09/1-800-657-4347-phone.png" width="500" alt="1-800-657-4347" /></a></div>
            </div>
            <?php
        //}    
    }
}

/**
 * Display Product Search and Phone Number
 * @since  1.0.0
 * @uses  is_woocommerce_activated() check if WooCommerce is activated
 * @return void
 */
if ( ! function_exists( 'storefront_product_search_and_phone' ) ) {
	function storefront_product_search_and_phone() {
	
	}
}

// Show empty categories
add_filter( 'woocommerce_product_subcategories_hide_empty', 'show_empty_categories', 10, 1 );
function show_empty_categories ( $show_empty ) {
    $show_empty  =  true;
    // You can add other logic here too
    return $show_empty;
}

// Display all products per page (just put in a big number). Goes in functions.php
//add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 9999;' ), //20 );

/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 9999;
  return $cols;
}

// Remove (Free) from shipping option
add_filter( 'woocommerce_cart_shipping_method_full_label', 'remove_local_pickup_free_label', 10, 2 );
function remove_local_pickup_free_label($full_label, $method){
    if( $method->id == 'local_pickup' )
    $full_label = str_replace("(Free)","",$full_label);
  return $full_label;
}

//show quantity available on the category archive pages
add_action('woocommerce_after_shop_loop_item_title', 'show_stock');
function show_stock() {
  global $product;
  if ( $product->stock ) { // if manage stock is enabled 
    if ( number_format($product->stock,0,'','') < 3 ) { // if stock is low
      echo '<div class="remaining">Only ' . number_format($product->stock,0,'','') . ' left in stock!</div>';
    } else {
    echo '<div class="remaining">' . number_format($product->stock,0,'','') . ' in stock</div>'; 
    }
  }
}

add_action( 'woocommerce_after_shop_loop_item_title', 'shop_sku' );
function shop_sku(){
global $product;
echo '<span itemprop="productID" class="sku">SKU: ' . $product->sku . '</span><br>';
}

 
/**
 * This adds the new unit of measure to the WooCommerce admin
 */
function add_woocommerce_dimension_unit_door( $settings ) {

	foreach ( $settings as &$setting ) {

		if ( 'woocommerce_dimension_unit' == $setting['id'] ) {
			$setting['options']['door'] = __( 'door' );  // new unit
		}
	}

	return $settings;
}
add_filter( 'woocommerce_products_general_settings', 'add_woocommerce_dimension_unit_door' );

/**
 * This adds the new unit of measure to the WooCommerce admin
 */
function add_woocommerce_dimension_unit_none( $settings ) {

	foreach ( $settings as &$setting ) {

		if ( 'woocommerce_dimension_unit' == $setting['id'] ) {
			$setting['options'][''] = __( '' );  // new unit
		}
	}

	return $settings;
}
add_filter( 'woocommerce_products_general_settings', 'add_woocommerce_dimension_unit_none' );